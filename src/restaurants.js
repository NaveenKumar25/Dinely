import React, { Component } from 'react';
import { View, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Left, Body, Right } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import Restaurants from './restaurantsList.js'

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default class CardImageExample extends Component {
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Content>
        <View style={styles.headerLeft}>
                     <TouchableOpacity onPress={() => navigate('Restaurants', {cat:'category one'})}>
                     <Icon name="arrow-left" size={18} color='#fff' />
                     </TouchableOpacity>
                  </View>
                  <View style={styles.headerRight}>
                     <TouchableOpacity onPress={() => navigate('Home', {cat:'category one'})}>
                     <Icon name="settings" size={18} color="#fff" />
                     </TouchableOpacity>
                  </View>
          {/* <Card>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: 'Image URL'}} />
                <Body>
                  <Text>NativeBase</Text>
                  <Text note>GeekyAnts</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'Image URL'}} style={{height: 200, width: null, flex: 1}}/>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="thumbs-up" />
                  <Text>12 Likes</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                  <Icon active name="chatbubbles" />
                  <Text>4 Comments</Text>
                </Button>
              </Body>
              <Right>
                <Text>11h ago</Text>
              </Right>
            </CardItem>
          </Card> */}
          <View>                       
                <Image source={require('./../images/restaurantImage1.jpeg')} style={styles.profileImage}/>
              </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  headerLeft:{
    position: 'absolute',
    top: 20,
    left: 20,
    zIndex: 1,
    backgroundColor: 'transparent'
},

headerRight:{
    position: 'absolute',
    top: 20,
    left: deviceWidth/1.1,
    zIndex: 1,
    backgroundColor: 'transparent'
},
profileImage:{
  height: 220,
  width: deviceWidth
}
})