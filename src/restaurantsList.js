import React, { Component } from 'react';
import {
AppRegistry,
StyleSheet,
Text,
View,
Image,
FlatList,
TouchableOpacity,
Dimensions
} from 'react-native';
import { Container, Header, Content, Left, Body, Right, Button, Title, Subtitle, Card, CardItem, Thumbnail, StyleProvider  } from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import getTheme from './../native-base-theme/components';
import material from './../native-base-theme/variables/material';

const numColumns = 1;
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

export default class HeaderTitleSubtitleExample extends Component {
static navigationOptions = {
title: 'Restaurants',
};
state = {
data: []
};
componentDidMount = () => {
fetch('https://demo2970366.mockable.io/restos/collections', {
method: 'GET'
})
.then((response) => response.json())
.then((responseJson) => {
console.log(responseJson);
this.setState({
data: responseJson
})
})
.catch((error) => {
console.error(error);
});
};
render() {
const { navigate } = this.props.navigation;
return (
  <StyleProvider style={getTheme(material)}>
<Container>
   <View style={styles.buckets}>
      <Header>
         <Left>
            <Button transparent onPress={() =>
               navigate('Home', {cat:'category one'})}>
               <Icon name="arrow-left" size={18} color="#fff" />
            </Button>
         </Left>
         <Body>
            <Title>BANJARA HILLS</Title>
         </Body>
         <Right>
            <Button transparent>
            <Icon name="filter" size={18} color="#fff" />
            </Button>
         </Right>
      </Header>
      <CardItem>
                  <Left style={{ marginRight:0}}>
                     <Body>
                        <Text style={ styles.heading }>HAND PICKED</Text>
                        <Text style={ styles.subHeading }>for you</Text>
                     </Body>
                  </Left>
                  <Right>
                    <Text style={ styles.restaurantsCount}>19</Text>
                  </Right>
               </CardItem>

      {/* <View>
      <View>
       <Text style={ styles.heading }>HAND PICKED</Text>
       <Text style={ styles.subHeading }>for you</Text>
      </View>
       <View>
          <Text style={ styles.restaurantsCount}>19</Text>
      </View>

      </View> */}
      <FlatList
      style={{}}
      data={this.state.data}
      renderItem={({item}) => (
      <View >
         <Content>
            <Card style={styles.component}>
               <CardItem cardBody >
                {/* <Button transparent onPress={() => navigate('RestaurantsProfile', {cat:'category one'})}> */}
                <TouchableOpacity onPress={() => navigate('RestaurantsProfile', {cat:'category one'})}>
                  <Image source={{uri: item.imageUrl }} style={styles.restaurantsImage}/>
                  </TouchableOpacity>
                  {/* </Button>  */}
               </CardItem>
               <CardItem>
                  <Left>
                     <Body>
                        <Text style={styles.itemName}>{item.name}</Text>
                        <Text style={styles.itemDesc}>{item.knownFor}</Text>
                         <Text style={{marginTop: 15}}>{item.cuisines}</Text>
                     </Body>
                  </Left>
                  <Right>
                     <Image source={{uri: item.visitedFriends[1].imageUrl }} style={{height: 25, width: 25, flex: 1, borderRadius: 12, position: 'absolute', top: -32, left: 65}}/> 
                     <Image source={{uri: item.visitedFriends[1].imageUrl }} style={{height: 25, width: 25, flex: 1, borderRadius: 12, position: 'absolute', top: -32,left: 80}}/> 
                     <Image source={{uri: item.visitedFriends[1].imageUrl }} style={{height: 25, width: 25, flex: 1, borderRadius: 12, position: 'absolute', top: -32,left: 100}}/> 
                     <Text style={{fontSize: 11, position: 'absolute', left: 65, backgroundColor: 'transparent',}}>{ item.visitedFriends.length} FRIENDS</Text>
                  </Right>
               </CardItem>
            </Card>
         </Content>
      </View>
      )}
      keyExtractor={item => item.id}
      numColumns= {numColumns} />
   </View>
</Container>
</StyleProvider>
);
}
}
const styles = StyleSheet.create({
heading: {
fontSize: 26,
letterSpacing: 1.1,
fontWeight: 'bold', 
width: 220,
},
subHeading: {
fontSize: 26,
letterSpacing: 1.1,
color: '#787575',
},
restaurantsCount: {
fontSize: 94,
color: '#787575',
},
itemName: {
fontSize: 18,
letterSpacing: 1.4,
width: 200,
},
itemDesc: {
fontSize: 14,
color: '#787575',
width: 230,
},
restaurantsImage: {
//marginRight: deviceWidth/15.63,
//marginLeft: deviceWidth/15.63,
marginTop: 0,
height: 200,
width: deviceWidth/1.147,
marginLeft: deviceWidth/17,
},
component:{
  marginTop: 0,
  marginBottom: 0
}

})