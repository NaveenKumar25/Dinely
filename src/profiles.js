import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    FlatList,
    TouchableOpacity,
    Dimensions,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const Genre = [
    {
        id: 0,
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTW46Krh9QWgyADuSVDLhbNZFQ8Ll_hIugv7GOVtLlxalY0lrZPnA',
        name:'Knifeparty',
        cost:'@ 4,000',
        orders: 72,
        rewards: 1400,
        restaurants: 24,
        wishlist: 68,
        dinelyCash: 55,
        photos: '00',
    }
];
const numColumns =1;

export default class Profile extends Component {
    static navigationOptions = {
        title: 'Profile',
        headerBackTitle: 'back'
    };
    render() {
        const { params } = this.props.navigation.state;
         const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
               <ScrollView>
                  <View style={styles.headerLeft}>
                     <TouchableOpacity onPress={() => navigate('Home', {cat:'category one'})}>
                     <Icon name="arrow-left" size={18} color="#fff" />
                     </TouchableOpacity>
                  </View>
                  <View style={styles.headerRight}>
                     <TouchableOpacity onPress={() => navigate('Home', {cat:'category one'})}>
                     <Icon name="settings" size={18} color="#fff" />
                     </TouchableOpacity>
                  </View>
                  <View>
                     <FlatList
                        style={{}}
                        data={Genre}
                        renderItem={({item}) => (
                            <View>
                                 <View style={styles.segment1}>
                                    <TouchableOpacity style={styles.data}  onPress={() =>
                                       navigate('EqupmentScreen',Genre)}>
                                       <Image source={{uri: item.image}} style={styles.profileImage}/>
                                    </TouchableOpacity>
                                 </View>

                                 <View style={ styles.flagImageDivision }>
                                    <Image source={require('./../images/flagImage.png')} style={ styles.flagImage }/> 
                                    <Text style={ styles.rewards }>{item.rewards}</Text>
                                 </View>
                                 
                                 <View style={styles.segment2}>
                                    <View style={styles.segmentDivision}>
                                       <TouchableOpacity>
                                          <Text style={ styles.innerSegmentText }>{item.orders}</Text>
                                          <Text style={ styles.innerSegmentText2 }>ORDERS</Text>
                                       </TouchableOpacity>
                                    </View>
                                    <View style={styles.segmentDivision}> 
                                       <TouchableOpacity>
                                          <Text style={ [styles.innerSegmentText,{marginLeft: deviceWidth/17.45}]}>{item.restaurants}</Text>
                                          <Text style={ [styles.innerSegmentText2, {marginLeft: deviceWidth/17.45}]}>RESTAURANTS</Text> 
                                       </TouchableOpacity>
                                    </View>
                                    <View style={[styles.segmentDivision,{ alignItems: 'flex-end'}]}>
                                       <TouchableOpacity>
                                          <Text style={ [styles.innerSegmentText,{ marginRight: deviceWidth/15.63 }]}>{item.wishlist}</Text>
                                          <Text style={ styles.innerSegmentText2 }>WISH LIST</Text>
                                       </TouchableOpacity>
                                    </View>
                                 </View>
                
                                 <View style={styles.segment3}>
                                    <View style={styles.segment31}>
                                       <View style={styles.innerSegmentsParts}>
                                          <Text style={ styles.segmentTitleLine1 }>LEADER</Text>
                                          <Text style={ styles.segmentTitleLine2 }>board</Text>
                                       </View>
                                       <View style={styles.innerSegmentsParts}>
                                          <Icon name='lock' size={50} color='#fff' style={ styles.lockIcon } />
                                       </View>
                                       <View style={styles.segmentDivision}>
                                          <View>
                                             <Image source={require('./../images/rankers.png')} style={ styles.topProfileImage}/> 
                                          </View>
                                       </View>
                                    </View>
                                    <View style={ styles.segment32 }>
                                    <Text style={ styles.segmentDescription }>ADD ATLEAST FIVE FRIENDS TO</Text>
                                       <Text style={{fontSize: 14, color: '#787575'}}>UNLOCK LEADER BOARD</Text>
                                    </View>
                                    <View style={styles.segment33}>
                                       <TouchableOpacity>
                                          <Text style={ styles.segmentLinks }>ADD FRIENDS</Text>
                                       </TouchableOpacity>
                                    </View>
                                 </View>

                                 <View style={styles.segment4}>
                                    <View style={styles.segment41}>
                                       <View style={styles.innerSegmentsParts}>
                                          <Text style={ styles.segmentTitleLine1 }>FOOD</Text>
                                          <Text style={ styles.segmentTitleLine2 }>pie</Text>
                                       </View>
                                       <View style={styles.innerSegmentsParts}>
                                          <Icon name='lock' size={50} color='#fff' style={ styles.lockIcon } />
                                       </View>
                                    </View>
                                    <View style={styles.segment42}>
                                       <Image source = {require('./../images/pieChart.png')} style={ styles.pieChart }/>
                                       <View style={ styles.restaurantsDesc }>
                                          <Text style={ styles.restaurantsDescText }>VISIT ATLEAST TEN RESTAURANTS</Text>
                                          <Text style={ styles.restaurantsDescText }>TO UNLOCK FOOD PIE</Text>
                                       </View>
                                    </View>
                                 </View>

                                 <View style={styles.segment5}>
                                    <View style={ styles.segmentFivePart1 }>
                                       <View style={ styles.segmentFivePart1_1}>
                                          <Text style={ styles.segmentTitleLine1 }>DINELY</Text>
                                          <Text style={ styles.segmentTitleLine2 }>cash</Text>
                                       </View>
                                       <View style={ styles.segmentFivePart1_2 }>
                                          <Text style={ styles.dinelyCount }>{item.dinelyCash}</Text>
                                       </View>
                                    </View>
                                    <View style={ styles.segmentFivePart2 }>
                                       <TouchableOpacity style={{marginTop: 10}}>
                                          <Text style={ styles.segmentLinks }>VIEW STATEMENT</Text>
                                       </TouchableOpacity>
                                    </View>
                                 </View>

                                 <View style={styles.segment6}>
                                    <View style={{ flex: 1, flexDirection: 'row',height: deviceHeight*8/6}}>
                                       <View style={{alignItems: 'flex-start', width: deviceWidth/2}}>
                                          <Text style={ styles.segmentTitleLine1 }>PHOTO</Text>
                                          <Text style={ styles.segmentTitleLine2 }>gallery</Text>
                                       </View>
                                       <View style={{ alignItems: 'flex-end',width: deviceWidth/2}}>
                                          <Text style={ styles.dinelyCount }>{item.photos}</Text>
                                       </View>
                                    </View>
                                    <View style={{alignItems: 'center', height: deviceHeight/5}}>
                                       <Text style={ styles.segmentDescription }>CAPTURE ALL YOUR FOOD</Text>
                                       <Text style={{color: '#787575'}}>MEMORIES HERE</Text>
                                       <TouchableOpacity style={{marginTop: 20}}>
                                          <Text style={ styles.segmentLinks }>UPLOAD PHOTO</Text>
                                       </TouchableOpacity>
                                    </View>
                                 </View>
                              </View>
                            )}
                        keyExtractor={item => item.id}
                        numColumns= {numColumns} />
                    <Text style={styles.userName}>Sandeep</Text>
                  </View>
               </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
          flex:1,
          flexDirection: 'column',
    },
    headerLeft:{
        position: 'absolute',
        top: 20,
        left: 20,
        zIndex: 1,
        backgroundColor: 'transparent'
    },
    backArrow: {
        color: '#fff',
    },
    headerRight:{
        position: 'absolute',
        top: 20,
        left: deviceWidth/1.1,
        zIndex: 1,
        backgroundColor: 'transparent'
    },
    segment1:{
           width: deviceWidth, 
           height: deviceHeight/2.2, 
           backgroundColor: 'powderblue',
    },
    segmentTitleLine1:{
          color: '#fff',
          fontSize: 26,
          marginLeft: deviceWidth/15.63,
          marginTop: deviceWidth/15.63,
          fontWeight: 'bold'
   },  
   segmentTitleLine2:{
          color: '#fff',
          fontSize: 26,
          marginLeft: deviceWidth/15.63,
   },
   segmentDescription:{
         fontSize: 14, 
         color: '#787575', 
         marginTop: 25
   },
    segment2:{
           flex: 1,
           flexDirection: 'row',
           width: deviceWidth, 
           height: deviceHeight/6.6, 
           backgroundColor: 'rgb(10, 10, 10)',
    },
    innerSegmentText:{
           color: '#fff',
           fontSize: 50,
           fontWeight: 'bold',
           marginTop: 10,
           marginLeft: deviceWidth/15.63,
    },
    innerSegmentText2:{
           color: '#fff',
           fontSize: 12,
           paddingTop: 5,
           marginLeft: deviceWidth/15.63,
    },
    segmentDivision:{
          width: deviceWidth/3, 
    },
    
    segment3:{
        flex: 1,
        flexDirection: 'column',
           width: deviceWidth, 
           height: deviceHeight/3.1, 
           backgroundColor: 'rgb(24, 24, 24)',
    },
    segment31:{
          flex: 1,
          flexDirection: 'row',
          height: deviceHeight*3/18, 
    },
    innerSegmentsParts:{
         width: deviceWidth/2.5
    },
    segment32:{
            height: deviceHeight*3/30, 
           alignItems: 'center',
           justifyContent: 'center',
    },
    segment33:{
            height: deviceHeight*3/30, 
            alignItems: 'center',
           justifyContent: 'center',
    },
    topProfileImage: {
           width: 100, 
           height: 80, 
           marginTop: deviceWidth/20,
           marginLeft: -deviceWidth/15

    },
    lockIcon:{
          marginLeft: deviceWidth/20, 
          marginTop: deviceWidth/10
    },
    segment4:{
           flex:1,
           flexDirection: 'column',
           width: deviceWidth, 
           height: deviceHeight/2.5, 
           backgroundColor: 'rgb(32, 32, 32)',
    },
    segment41:{
          flex: 1,
          flexDirection: 'row',
          height: deviceHeight*3/20, 
    },
    segment42:{
           height: deviceHeight*3/10, 
           alignItems: 'center',
           justifyContent: 'center',
    },
    segment5:{
           flex: 1,
           flexDirection: 'column',
           width: deviceWidth, 
           height: deviceHeight/4, 
           backgroundColor: 'rgb(0, 0, 0)',
    },
    segmentFivePart1_1:{
           alignItems: 'flex-start',
           width: deviceWidth/2
    },
    segmentFivePart1_2:{
           alignItems: 'flex-end',
           width: deviceWidth/2
    },
    segmentFivePart1:{
           flex: 1, 
           flexDirection: 'row',
           height: deviceHeight*8/3
    },
    segmentFivePart2:{
           alignItems: 'center', 
           height: deviceHeight/12
    },
    dinelyCount:{
           color: '#787575',
           fontSize: 94, 
           fontWeight: '700',
           marginRight: deviceWidth/15.63, 
           marginTop: 8
    },
    segment6:{
           width: deviceWidth, 
           height: deviceHeight/2.2, 
           backgroundColor: 'rgb(24, 24, 24)',
    },
    segmentLinks:{
           color: 'rgb(241, 106, 108)', 
           fontSize: 16
    },
    restaurantsDesc: {
           position: 'absolute', 
           backgroundColor: 'transparent', 
           top: 150, 
           justifyContent: 'center',
           alignItems: 'center'
    },
    restaurantsDescText:{
          fontSize: 14, 
          color: '#787575'
    },
    pieChart:{
          width: 175, 
          height: 150, 
          opacity: 0.3
    },
    userName:{
        position: 'absolute',
        top: deviceHeight/2.8,
        left: deviceWidth/16,
        backgroundColor: 'transparent',
        color: '#fff',
        fontSize: 34,
        fontWeight: '100',
        zIndex: 1,
        letterSpacing: 1.5,
    },
    profileImage:{
        height: deviceHeight/2.1 ,
        width: deviceWidth,
        opacity: 0.8
    },
    flagImageDivision:{
        position: 'absolute',
        top: deviceHeight/3.1,
        left: deviceWidth/1.2 ,
        zIndex: 1
    },
    flagImage:{
       width: 60,
       height: 50,
    },
    rewards:{
       marginTop: -10,
       marginLeft: -10,
       fontSize: 16,
       color: '#fff',
       backgroundColor: 'transparent',
    }

})

//AppRegistry.registerComponent('AboutUs', () => AboutUs);