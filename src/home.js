import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    TouchableHighlight,
    StyleSheet,
    FlatList,
    ScrollView,
    TextInput,
    Image,
    Dimensions,
    Modal
} from 'react-native';
import SearchBar from 'react-native-searchbar';
import { Container, Header, Item, Input, Icon, Button, List, ListItem,Card, CardItem, Body, Thumbnail , Right} from 'native-base';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const items = [
    1337,
    'janeway',
    {
        lots: 'of',
        different: {
            types: 0,
            data: false,
            that: {
                can: {
                    be: {
                        quite: {
                            complex: {
                                hidden: [ 'gold!' ],
                            },
                        },
                    },
                },
            },
        },
    },
    [ 4, 2, 'tree' ],
];

const Genre = [
    {
        id: 0,
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR915QIK8KUPMr7xTv_jRPBfW6QehxZE-iSPXKpn4k88wwVQZZgaw',
        name:'Knifeparty',
        cost:'@ 4,000'
    },
    {
        id: 1,
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ3oDyMvIOkgn8wNpnNrUq_JyMM7Wy65naQIBQEkXAFk69RwuwiiA',
        name:'Mustafa Zahid',
        cost:'@ 5,000'
    },
    {
        id: 2,
        image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRztA4l8weSpXdTL4-eL5YuCp9p337hZa4md80OFJ-w7jq2nNAc',
        name:'Sia',
        cost:'@ 7,000'
    },
    {
        id: 3,
        image:  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRR8hSiPCk2v0Kh-3oiFVKaBRyCmHAFfv7amga3NwMEMUKwC6OK',
        name:'Adnan Sami',
        cost:'@ 4,000'
    },
    {
        id: 3,
        image:  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAXTn_jOccJQ3h7RGvu0SF03jOW_JsZBQrkTFuvWNlTMx8vSWtpQ',
        name:'Adnan Sami',
        cost:'@ 4,000'
    },
    {
        id: 3,
        image:  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSNhZDOBJOzPYUNoR5QgaBw6LOyPalR6uw_PSfb9depUKuPVJK',
        name:'Adnan Sami',
        cost:'@ 4,000'
    }
];
const numColumns = 2;

export default class Home extends Component {
    static navigationOptions = {
        //title: 'Home',
       // headerBackTitle: 'Back',
        headerMode: 'none'
    };

    constructor(props) {
        super(props);
        this.state = {
            items,
            modalVisible: false,
            results: []
        };
        this._handleResults = this._handleResults.bind(this);
    }

    _handleResults(results) {
        this.setState({ results });
    }

    toggleModal(visible) {
        this.setState({ modalVisible: visible });
    }


    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.headerView}>
                    <Text style={styles.headerText}>Whats Cooking</Text>
                    <Text style={styles.headerText}>Today?</Text>
                    </View>
                    <View>
                        {
                            this.state.results.map((result, i) => {
                                return (
                                    <Text key={i}>
                                        {typeof result === 'object' && !(result instanceof Array) ? 'gold object!' : result.toString()}
                                    </Text>
                                );
                            })
                        }
                    </View>
                    <View searchBar style={styles.searchArea}>

                        <TouchableHighlight onPress = {() => {this.toggleModal(true)}} style={{height: 50}}>
                            {/*<Input />*/}
                            <Icon name="ios-search" style={{marginLeft: deviceWidth/1.2, marginTop:10, color: '#787575'}}/>
                        </TouchableHighlight>

                    </View>

                    <View>
                        <Modal animationType = {"slide"} transparent = {false}
                               visible = {this.state.modalVisible}
                               onRequestClose = {() => { console.log("Modal has been closed.") } }>
                            <View style = {styles.modal}>
                                <Text style = {styles.text}>Modal is open!</Text>

                                <TouchableHighlight onPress = {() => {
                                    this.toggleModal(!this.state.modalVisible)}}>

                                    <Text style = {styles.text}>Close Modal</Text>
                                </TouchableHighlight>
                            </View>
                        </Modal>
                    </View>

                    <View style={styles.tabs}>
                        <TouchableOpacity onPress={() => this.searchBar.show()}>
                            <View style={styles.tabLinks}><Text style={styles.linkText}>FEED</Text></View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.searchBar.hide()}>
                            <View style={styles.activeTabLink} ><Text style={styles.activeLinkText}>HOME</Text></View>
                        </TouchableOpacity>
                        <TouchableOpacity title = 'ProfileScreen' color="green"
                                          onPress={() => navigate('Profile', {cat:'category one'})}>
                            <View style={styles.tabLinks}><Text style={styles.linkText}>PROFILE</Text></View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.buckets}>
                        <FlatList
                            style={{marginTop: 20}}
                            data={Genre}
                            renderItem={({item}) => (
                                <View >
                                    <TouchableOpacity style={styles.data}  onPress={() => navigate('Restaurants', {cat:'category one'})}>
                                        <Image source={{uri: item.image}} style={styles.bucketImages}/>
                                        {/*<Text style={{fontSize:15,fontWeight:'bold'}}>{item.name}</Text>*/}
                                        {/*<Text>{item.cost}</Text>*/}
                                    </TouchableOpacity>
                                </View>
                            )}
                            keyExtractor={item => item.id}
                            numColumns= {numColumns} />
                    </View>
                    <View style={{backgroundColor: 'rgb(24, 24, 24)'}}>
                        <View style={{marginTop: 35, marginLeft: 18, marginBottom: 35, }}>
                            <TouchableOpacity style={styles.data}  onPress={() =>
                                navigate('EqupmentScreen',Genre)}>
                                <Text style={{color: 'rgb(241, 106, 108)', fontSize: 22, letterSpacing: 1.5,}}>SHOW RESTAURANTS</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#000',
    },
    headerView:{
        marginTop: 10
    },
    headerText:{
        fontSize: 30,
        letterSpacing: 2,
        color: '#fff',
        fontWeight: 'bold',
        padding: 5,
        marginLeft: 15,
    },
    searchArea:{
        flex: 0.5,
        justifyContent: 'center',
        backgroundColor: 'rgb(50, 50, 50)',
        //width: 50,
        //height: 50,
        marginTop: 30,
        marginLeft: 15,
        marginRight: 15,
    },
    SearchBar:{
        //flex: 1,
        //justifyContent: 'center',
        height: 100,
    },
    tabs:{
        flexDirection:'row',
        justifyContent: 'space-between'
    },
    tabLinks:{
        backgroundColor: '#000',
        height: 60,
        width: 125,
        marginTop: 15,
    },
    activeTabLink:{
        backgroundColor: '#000',
        height: 60,
        width: 125,
        marginTop: 15,
        borderBottomColor: '#fff',
        borderBottomWidth: 3,
    },
    linkText: {
        paddingTop: 25,
        paddingLeft: 35,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#787575',
    },
    activeLinkText:{
        paddingTop: 25,
        paddingLeft: 35,
        fontSize: 16,
        fontWeight: 'bold',
        color: '#fff',
    },
    buckets:{
        backgroundColor: 'rgb(24, 24, 24)',
        //margin: 10,
        flexDirection: 'column',
        alignItems: 'center',
    },
    bucketImages:{
        width: deviceWidth/2.35,
        height: deviceHeight/3,
        marginLeft: 10,
        marginRight: 10,
        marginTop: 27,
    },
    input:{
        height: 60,
    },


    modal: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#28C1BE',
        padding: 100
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    }
});