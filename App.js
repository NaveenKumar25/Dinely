/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View
} from 'react-native';

import Home from './src/home.js'
import Profile from './src/profiles.js'
import Restaurants from './src/restaurantsList.js'
import RestaurantsProfile from './src/restaurants.js'

import { StackNavigator } from 'react-navigation';

const App = StackNavigator({
        Home: { screen: Home,
            navigationOptions: {
                //gesturesEnabled: false,
                header: false,
            }
    },
        Profile: { screen: Profile,
            navigationOptions: {
                //gesturesEnabled: false,
                header: true,
                headerLeft: 'back',
            }
        },
        Restaurants: { screen: Restaurants,
            navigationOptions: {
                //gesturesEnabled: false,
                header: false,
            }
    },
    RestaurantsProfile: { screen: RestaurantsProfile,
            navigationOptions: {
                //gesturesEnabled: false,
                header: false,
            }
    },
    }
);


// export default class App extends Component {
//     render() {
//         return (
//             <Home/>
//         );
//     }
// }

const styles = StyleSheet.create({

});

export default App;